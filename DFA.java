import java.util.*;

public class DFA {
	private LinkedList<State> dfa;
	
	public DFA () {
		this.setDfa(new LinkedList<State> ());
		this.getDfa().clear();
	}

	public LinkedList<State> getDfa() {
		return dfa;
	}

	public void removeUnread() {
		Set <State> unreadStates = new HashSet<>();
		for(int i=1;i<dfa.size();i++){
			boolean isUnread = true;
			for ( State is : dfa) {
				if( dfa.get(i).getStateID() != is.getStateID()) {
					for (Map.Entry<Character, ArrayList<State>> entry : is.getNextState().entrySet()) {
						for (State state : entry.getValue()) {
							if(state.getStateID() == dfa.get(i).getStateID())
								isUnread = false;
							break;
						}
					}
				}
			}
			if(isUnread) {
				unreadStates.add(dfa.get(i));
			}
		}
		dfa.removeAll(unreadStates);
	}
	public  void minimization(){
		removeUnread();
		int [][] matrix = new int[dfa.size()][dfa.size()];
		int k = dfa.size();
		for (int i=1;i<dfa.size();i++){
			k--;
			for (int j=0;j<dfa.size()-k;j++){
				if ((dfa.get(i).isAcceptState() && !dfa.get(j).isAcceptState()) || (!dfa.get(i).isAcceptState() && dfa.get(j).isAcceptState())) {
					matrix[i][j] = 1;
				}
			}
		}

		boolean chanched = true;
		while(chanched) {
		    chanched = false;
            k = dfa.size();
            for (int i = 1; i < dfa.size(); i++) {
                k--;
                for (int j = 0; j < dfa.size() - k; j++) {
                    if (matrix[i][j] != 1) {
                        for (Character c : RegularExpression.input) {
                            int rowId = dfa.get(i).getNextState().get(c).get(0).getStateID();
                            int colId = dfa.get(j).getNextState().get(c).get(0).getStateID();
                            if (matrix[rowId][colId] == 1 || matrix[colId][rowId] == 1) {
                                matrix[i][j] = 1;
                                chanched = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
		k = dfa.size();
		List<Set<State>> uncheckedStates = new ArrayList<>();
		LinkedList<State> minDfa = new LinkedList<>();
		for (int i=1;i<dfa.size();i++){
			k--;
			for (int j=0;j<dfa.size()-k;j++){
				if (matrix [i][j] != 1) {
					Set<State> newList = new HashSet<>();
					newList.add(dfa.get(i));
					newList.add(dfa.get(j));
					uncheckedStates.add(newList);
				}
			}
		}
		for (State s : dfa) {
            chanched = false;
            for(Set<State> states : uncheckedStates) {
		        for(State state : states) {
		            if(s.getStateID() == state.getStateID()) {
		                chanched = true;
                    }
                }
            }
            if(!chanched) {
                Set<State> newList = new HashSet<>();
                newList.add(s);
                uncheckedStates.add(newList);
            }
        }
		chanched = true;
		int id = -1;
		int index = -1;
		int pindex = -1;
		while(chanched) {
		    chanched = false;
            for (int i = 0; i < uncheckedStates.size(); i++) {
                for (State s : uncheckedStates.get(i)) {
                    for (int j = 0; j < uncheckedStates.size(); j++) {
                        if (i != j) {
                            for (State s2 : uncheckedStates.get(j)) {
                                if((s2.getStateID() == s.getStateID())&& !chanched) {
                                    chanched = true;
                                    id = s.getStateID();
                                    index = i;
                                    pindex = j;
                                }
                            }
                        }
                    }
                }
            }
            if (chanched) {
                int finalPindex = pindex;
                uncheckedStates.get(index).forEach(x-> uncheckedStates.get(finalPindex).add(x));
                uncheckedStates.remove(index);
            }
        }


		for (int i = 0; i < uncheckedStates.size(); i++) {
			minDfa.add(new State(i));
		}

        int startIndex = dfa.getFirst().getStateID();
		int hereStart = 0;
        for (int i = 0; i < uncheckedStates.size(); i++) {
            for (State s : uncheckedStates.get(i)) {
                if(startIndex == s.getStateID()) {
                    hereStart = i;
                }
            }
        }
        if(hereStart !=0) {
            Set<State> startState = uncheckedStates.get(hereStart);
            Set<State> oldState = uncheckedStates.get(0);
            uncheckedStates.set(0,startState);
            uncheckedStates.set(hereStart,oldState);
        }
        boolean isFinal = false;
        for (int i =0; i<uncheckedStates.size(); i++) {
            isFinal = false;
            for (Character c : RegularExpression.input) {
                chanched = false;
                int stateId = uncheckedStates.get(i).iterator().next().getNextState().get(c).get(0).getStateID();
                for (int j = 0; j < uncheckedStates.size(); j++) {
                    for (State s : uncheckedStates.get(j)) {
                        if(stateId == s.getStateID() && !chanched) {
                            hereStart = j;
                            chanched = true;
                        }
                    }
                }
                minDfa.get(i).addTransition(minDfa.get(hereStart),c);
                for (State s : uncheckedStates.get(i)) {
                    if(s.isAcceptState()) {
                        isFinal = true;
                    }
                }
                if(isFinal) {
                    minDfa.get(i).setAcceptState(true);
                }
            }
        }
            for (State s : minDfa) {
                if(s.getNextState().containsKey('e')) {
                    s.getNextState().remove('e');
                }
            }
			dfa = minDfa;
	}
	public void setDfa(LinkedList<State> nfa) {
		this.dfa = nfa;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for(State state : dfa) {
			str.append("\n");
			str.append(state.getStateID());
            if(state.isAcceptState()) {
                str.append(" Accept ");
            }
			str.append(": ");
			for (Map.Entry<Character, ArrayList<State>> entry : state.getNextState().entrySet()) {
				str.append("char: ".concat(entry.getKey().toString()));
				StringBuilder states = new StringBuilder();
				entry.getValue().forEach(x->states.append(x.getStateID()));
				str.append(" states: ".concat(states.toString()).concat(" "));
			}
		}
		return  str.toString();
	}
}
