import java.util.ArrayList;
import java.util.Scanner;

public class ExpressionReader {
	private static Scanner s;
	private static String regular;
	
	// Reads all the expressions in this arrayList
	private static ArrayList<String> expressions = new ArrayList<String>();
	
	// stores de NFA
	private static NFA nfa;
	
	// stores the DFA
	private static DFA dfa;

	public static void main(String[] args) {

		s = new Scanner(System.in);

		regular = s.next();
		System.out.println(regular);
		boolean read = true;
		while (read) {
			String readString = s.next();
			if (readString.equals("STOP")) {
				read = false;
			} else {
				expressions.add(readString);
			}
		}
		setNfa(RegularExpression.generateNFA(regular));

		setDfa(RegularExpression.generateDFA(getNfa()));


		System.out.println(getNfa().toString());
		System.out.println(getDfa());
		getDfa().minimization();
		System.out.println(getDfa());

		System.out.println();
		for (String str : expressions) {
            System.out.println("Expresion: "+str);
			if (ValidateExpression.validate(getDfa(), str)) {
				System.out.println("yes");
			} else {
				System.out.println("no");
			}
		}
	}
	public static NFA getNfa() {
		return nfa;
	}

	public static void setNfa(NFA nfa) {
		ExpressionReader.nfa = nfa;
	}

	public static DFA getDfa() {
		return dfa;
	}

	public static void setDfa(DFA dfa) {
		ExpressionReader.dfa = dfa;
	}
}
