import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

public class NFA {
	private LinkedList<State> nfa;
	
	public NFA () {
		this.setNfa(new LinkedList<State> ());
		this.getNfa().clear();
	}

	@Override
	public String toString(){
		StringBuilder str = new StringBuilder();
		for(State state : nfa) {
			str.append("\n");
			str.append(state.getStateID());
			if(state.isAcceptState()) {
				str.append(" Accept ");
			}
			str.append(": ");
			for (Map.Entry<Character, ArrayList<State>> entry : state.getNextState().entrySet()) {
				str.append("char: ".concat(entry.getKey().toString()));
				StringBuilder states = new StringBuilder();
				entry.getValue().forEach(x->states.append(x.getStateID()+" "));
				str.append(" states: ".concat(states.toString()));
			}
		}
		return  str.toString();
	}
	public LinkedList<State> getNfa() {
		return nfa;
	}

	public void setNfa(LinkedList<State> nfa) {
		this.nfa = nfa;
	}
}
