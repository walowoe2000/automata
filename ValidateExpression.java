public class ValidateExpression {

	public static boolean validate(DFA dfa, String s) {
		
		State state = dfa.getDfa().getFirst();
		
		if (s.compareTo("e") == 0) {
			if (state.isAcceptState()) 	{	return true; 	}
			else						{	return false; 	}
			
		} else if (dfa.getDfa().size() > 0) {	

			for (int i = 0 ; i < s.length(); i++) {

				if (state == null) { break; }
				
				System.out.println(state.getNextState().get(s.charAt(i)).get(0).getStateID());
				state = state.getNextState().get(s.charAt(i)).get(0);
			}
			
			if (state != null && state.isAcceptState()) {	return true;	}
			else 										{	return false;	}
		
		} else {
			return false;
		}
		
	}
}
